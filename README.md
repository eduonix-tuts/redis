```bash
$ npm install
$ docker-compose up -d
```

Run Redis commands:
```bash
$ docker-compose exec redis redis-cli <command>
```