var express      = require('express');
var path         = require('path');
var logger       = require('morgan');
var bodyParser   = require('body-parser');
var redis        = require('redis');

var app = express();

// create client
var client = redis.createClient({
    host: 'redis'
});

const taskList = 'tasks';
const callList = 'call';

client.on('connect', function() {
    console.log('Redis sever connected');
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res)  {
    client.lrange(taskList, 0,-1, function(err, reply) {

        client.hgetall(callList, function(err, call) {
            res.render('index', {
                title: "Task list",
                tasks: reply,
                call: call
            });
        });
    });
});


app.post('/task/add', function(req, res) {
    var task = req.body.task;

    client.rpush(taskList, task, function(err, reply) {
        if (err) {
            console.error(err);
        } else {
            console.log('Task added:' + task);
        }

        res.redirect('/');
    })
});

app.post('/tasks/delete', function(req, res) {
    var tasksToDelete = req.body.tasks;

    client.lrange(taskList, 0, -1, function(err, tasks) {
        for (var i = 0, c = tasks.length ; i < c ; i++) {
            if (tasksToDelete.indexOf(tasks[i]) > -1) {
                client.lrem(taskList, 0, tasks[i], function(err, reply) {
                    if (err) {
                        console.error(err);
                    }
                });
            }
        }

        res.redirect('/');
    });
});

app.post('/call/add', function(req, res) {
    var newCall = {};

    newCall.name    = req.body.name;
    newCall.company = req.body.company;
    newCall.phone   = req.body.phone;
    newCall.time    = req.body.time;

    client.hmset(
        callList,
        {
            name: newCall.name,
            company: newCall.company,
            phone: newCall.phone,
            time: newCall.time
        },
        function(err, reply) {
            if (err) {
                console.error(err);
            }

            console.log(reply);

            res.redirect('/');
        }
    );
});

app.listen(3000);
console.log('Server started on port 3000');

module.exports = app;